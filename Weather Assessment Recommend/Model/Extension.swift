//
//  Extension.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 31/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import Foundation
import UIKit


// MARK: - UIViewController
extension UITableViewController {
    //prompt alert error
    //parameter string message
    func alertPopup(_ message: String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Float
extension Float {
    //to print float value with desire decimal place
    //parameter decimal place in string, e.g: '0.3'
    func formatData(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

// MARK: - Date
extension Date {
    //to get current day
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}
