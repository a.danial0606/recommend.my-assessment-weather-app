//
//  DataModel.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 31/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Contacts

// MARK: - Global variable
let prefs = UserDefaults.standard
let screenHeight = UIScreen.main.bounds.height
let screenWidth = UIScreen.main.bounds.width

// MARK: - Weather Data
struct WeatherDataModel: Decodable {
    var coord: CoordinateData
    var weather: [WeatherData]
    var base: String?
    var rain: String?
    var snow: String?
    var main: WeatherMainData
    var visibility: Int?
    var wind: WindData
    var clouds: CloudData
    var dt: Int
    var sys: SysData
    var timezone: Int?
    var id: Int
    var name: String
    var cod: Int?
  
}

struct CoordinateData: Decodable {
    var lon: Float?
    var lat: Float?
}

struct WeatherData: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
}

struct WeatherMainData: Decodable {
    var temp: Float?
    var feels_like: Float?
    var temp_min: Float?
    var temp_max: Float?
    var pressure: Int?
    var humidity: Int?
}

struct WindData: Decodable {
    var speed: Float?
    var deg: Int?
}

struct CloudData: Decodable {
    var all: Int?
}

struct SysData: Decodable {
    var type: Int?
    var id: Int?
    var country: String?
    var sunrise: Int?
    var sunset: Int?
}

struct WeatherDataView {
    var temperature: Float
    var weatherStatus: String
    var cityName: String
    var windSpeed: Float
    var humadity: Int
    var icon: String
    
    init(weatherDataModel: WeatherDataModel, weatherData: WeatherData, weatherMainData: WeatherMainData, windData: WindData) {
        self.cityName = weatherDataModel.name
        self.windSpeed = windData.speed ?? 0.0
        self.temperature = weatherMainData.temp ?? 0.0
        self.humadity = weatherMainData.humidity ?? 0
        self.weatherStatus = weatherData.description ?? ""
        self.icon = weatherData.icon ?? ""
    }
}

// MARK: - Nearest City Weather Data
struct NearestCityData: Decodable{
    var message: String
    var cod: String
    var count: Int
    var list: [WeatherDataModel]
}

