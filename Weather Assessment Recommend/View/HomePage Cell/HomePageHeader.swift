//
//  HomePageHeader.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 30/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit

protocol HomePageHeaderDelegate {
    func changeFormat(value: Int)
}

class HomePageHeader: UIView {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var segmentFormat: UISegmentedControl!
    
    var delegate: HomePageHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.segmentFormat.addTarget(self, action: #selector(switchFormat(_:)), for: .valueChanged)
    }
    
    func configureView(tempFormat: String){
        self.titleLbl.text = "Temperature Format:"
        
        switch tempFormat {
        case "celsius":
            self.segmentFormat.selectedSegmentIndex = 0
        default:
            self.segmentFormat.selectedSegmentIndex = 1
        }
    }
    
    @objc func switchFormat(_ sender: UISegmentedControl){
        self.delegate?.changeFormat(value: sender.selectedSegmentIndex)
    }
}
