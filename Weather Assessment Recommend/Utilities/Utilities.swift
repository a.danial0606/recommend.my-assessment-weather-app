//
//  Utilities.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 03/02/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import Foundation
import UIKit

//class to create func that use entire app
class Application {
    //set background image based on weather status
    class func getBackgroundImage(_ icon: String) -> String{
        switch icon {
        case "01d":
            return "clear_day.jpg"
        case "01n":
            return "clear_night.jpg"
        case "02d", "03d", "04d":
            return "cloudy_day.jpg"
        case "02n", "03n", "04n":
            return "cloudy_night.jpg"
        case "09d", "10d":
            return "raining_day.jpg"
        case "09n", "10n":
            return "raining_night.jpg"
        case "11d", "11n":
            return "thuderstorm.jpg"
        case "13d", "13n":
            return "snow.jpg"
        default:
            return "mist.jpg"
        }
    }
}
