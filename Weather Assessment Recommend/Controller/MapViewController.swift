//
//  MapViewController.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 31/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class MapViewController: UIViewController {

    @IBOutlet weak var mapFrame: MKMapView!
    
    private var cityData: NearestCityData?
    private var weatherDataOnClick: WeatherDataModel?
    private let sAppId = "fdf871cedaf3413c6a23230372c30a02"
    private var annotationClick = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //set tap long gesture for add coordinate annotation on map
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(getCoordinate(_:)))
        tap.minimumPressDuration = 0.3 // minimum hold tap is 0.3s
        self.mapFrame.addGestureRecognizer(tap)
        
        self.getNearestCityWeatherData{ (data) in
            self.cityData = data
            self.initalizeMapView{ (status) in
                if status {
                    self.mapViewDidFinishLoadingMap()
                }
            }
        }
    }
    
    // MARK: - Function
    //add annotation based on nearest city data
    func initalizeMapView(completion: @escaping ((Bool)-> ())){
        for i in 0..<(self.cityData?.list.count ?? 0) {
            let pinStore = MKPointAnnotation()
            pinStore.title = self.cityData?.list[i].name ?? ""
            for j in 0..<(self.cityData?.list[i].weather.count ?? 0) {
                pinStore.subtitle = self.cityData?.list[i].weather[j].description ?? ""
            }
            let lat = self.cityData?.list[i].coord.lat ?? 0.0
            let long = self.cityData?.list[i].coord.lon ?? 0.0
            pinStore.coordinate = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(long))
            
            self.mapFrame.addAnnotation(pinStore)
            self.mapFrame.showsUserLocation = true
        }
        completion(true)
    }
    
    //check if finish add annotation, show annotation
    func mapViewDidFinishLoadingMap() {
        DispatchQueue.main.async {
            self.mapFrame.showAnnotations(self.mapFrame.annotations, animated: true)
        }
    }

    // MARK: - Function Get Data
    func getNearestCityWeatherData(completion: @escaping ((_ cityData: NearestCityData)->())){
        let latitude = prefs.double(forKey: "sLatitude")
        let longitude = prefs.double(forKey: "sLongitude")
        let totalCity = 10
        let jsonUrlString = "https://api.openweathermap.org/data/2.5/find?lat=\(latitude)&lon=\(longitude)&cnt=\(totalCity)&appid=\(self.sAppId)"
        
        guard let url = URL(string: jsonUrlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                let data = try JSONDecoder().decode(NearestCityData.self, from: data)
               
               completion(data)
               
            }catch let error {
                print(error)
            }
            
        }.resume()
    }
    
    //get weather based onClick coordinate
    func getWeatherData(latitude: Double, longitude: Double, completion: @escaping ((_ weatherData: WeatherDataModel)->())){
        let jsonUrlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=\(self.sAppId)"
        
        guard let url = URL(string: jsonUrlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                let data = try JSONDecoder().decode(WeatherDataModel.self, from: data)
               
               completion(data)
               
            }catch let error {
                print(error)
            }
            
        }.resume()
    }
    
    // MARK: - Event Actions
    @objc func getCoordinate(_ sender: UILongPressGestureRecognizer){
        if (sender.state == .ended) {
            //remove previous annotation
            self.mapFrame.removeAnnotation(annotationClick)
            
            let location = sender.location(in: self.mapFrame)
            let coordinate = self.mapFrame.convert(location, toCoordinateFrom: self.mapFrame)
            
            self.getWeatherData(latitude: coordinate.latitude, longitude: coordinate.longitude){ (data) in
                self.weatherDataOnClick = data
                
                // Add annotation:
                //add UI in main thread only
                DispatchQueue.main.async {
                    self.annotationClick.title = "Lat: \(Float(coordinate.latitude).formatData(f: ".3")), Long: \(Float(coordinate.longitude).formatData(f: ".3"))"
                    self.annotationClick.subtitle = data.weather[0].description
                    self.annotationClick.coordinate = coordinate
                    self.mapFrame.addAnnotation(self.annotationClick) //add annotation
                    self.mapFrame.selectAnnotation(self.annotationClick, animated: false) //automatically show annotation view
                }
            }
        }
    }
}


// MARK: - MKMapViewDelegate Delegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let latFromPin = view.annotation!.coordinate.latitude
        let lonFromPin = view.annotation!.coordinate.longitude
        let point = MKPointAnnotation()
        point.coordinate = CLLocationCoordinate2DMake(latFromPin, lonFromPin)
        self.mapFrame.centerCoordinate = point.coordinate
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = UIButton(type:UIButton.ButtonType.infoDark) as UIButton
        } else {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotationTitle = view.annotation?.title else { return }
        
        let nibView = WeatherDetailModalViewController(nibName: "WeatherDetail", bundle: nil)
        nibView.modalPresentationStyle = .overCurrentContext
        
        if annotationTitle == self.annotationClick.title{
            guard let weatherData = self.weatherDataOnClick?.weather[0] else { return }
            guard let weatherDataModel = self.weatherDataOnClick else { return }
            guard let weatherMainData = self.weatherDataOnClick?.main else { return }
            guard let weatherWindData = self.weatherDataOnClick?.wind else { return }
            
            nibView.dataView = WeatherDataView(weatherDataModel: weatherDataModel, weatherData: weatherData, weatherMainData: weatherMainData, windData: weatherWindData)
        }else{
            let data = self.cityData?.list.filter{ $0.name.elementsEqual(annotationTitle ?? "") }

            guard let weatherData = data?[0].weather[0] else { return }
            guard let weatherDataModel = data?[0] else { return }
            guard let weatherMainData = data?[0].main else { return }
            guard let weatherWindData = data?[0].wind else { return }
            
            nibView.dataView = WeatherDataView(weatherDataModel: weatherDataModel, weatherData: weatherData, weatherMainData: weatherMainData, windData: weatherWindData)
        }
        
        self.present(nibView, animated: true, completion: nil)
    }
}
