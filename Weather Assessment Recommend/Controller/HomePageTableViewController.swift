//
//  HomePageTableViewController.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 30/01/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit
import CoreLocation

class HomePageTableViewController: UITableViewController {
    
    @IBOutlet weak var mapBtn: UIButton!
    
    private let sAppId = "fdf871cedaf3413c6a23230372c30a02"
    private var weatherData: WeatherDataModel?
    private var locationManager = CLLocationManager()
    private let tableRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add pulldown to refresh
        self.tableRefreshControl.addTarget(self, action:  #selector(refreshTable(_:)), for: .valueChanged)
        self.tableRefreshControl.tintColor = UIColor.init(named: "3899D8")
        
        //setup things
        self.setupTableView()
        self.setupCoreLocation()
        self.setupButton()
        
        //display loading animation
        let nibView = LoadingAnimationModal(nibName: "LoadingAnimation", bundle: nil)
        nibView.modalPresentationStyle = .overCurrentContext
        self.present(nibView, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //hide header by default
        self.tableView.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
    }
    
    // MARK: - Function
    func setupTableView() {
        self.tableView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "HomePageCell")
        self.tableView.refreshControl = self.tableRefreshControl
    }
    
    func setupButton(){
        self.mapBtn.addTarget(self, action: #selector(goToMap(_:)), for: .touchUpInside)
    }
    
    //setup core location and permission
    func setupCoreLocation(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            self.locationManager.startUpdatingLocation()
        }else{
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //get data
    func getWeatherData(completion: @escaping ((_ weatherData: WeatherDataModel)->())){
        let latitude = prefs.double(forKey: "sLatitude")
        let longitude = prefs.double(forKey: "sLongitude")
        let jsonUrlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=\(self.sAppId)"
        
        guard let url = URL(string: jsonUrlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                let data = try JSONDecoder().decode(WeatherDataModel.self, from: data)
               
               completion(data)
               
            }catch let error {
                print(error)
            }
            
        }.resume()
    }
    
    // MARK: - Event Actions
    @objc func refreshTable(_ sender: UIRefreshControl){
        self.getWeatherData{ (data) in
            self.weatherData = data
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableRefreshControl.endRefreshing()
            }
        }
    }
    
    @objc func goToMap(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Map", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //header for toggle temperature format
        let nib = UINib(nibName: "HomePageHeader", bundle: nil)
        let myNibView = nib.instantiate(withOwner: self, options: nil)[0] as! HomePageHeader
        myNibView.configureView(tempFormat: prefs.string(forKey: "sTemperatureFormat") ?? "")
        myNibView.delegate = self
        return myNibView
    }
       
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
          
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screenHeight - (self.navigationController?.navigationBar.frame.height ?? 0.0)// minus header height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell", for: indexPath) as! HomePageTableViewCell
        guard let data = self.weatherData else { return cell }
        cell.configureCell(dataWeather: WeatherDataView(weatherDataModel: data, weatherData: data.weather[indexPath.row], weatherMainData: data.main, windData: data.wind))
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - Core Location Delegate
extension HomePageTableViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // The user has given permission to your app
        if status == .authorizedWhenInUse || status ==  .authorizedAlways{
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        
        self.alertPopup( "We couldn't get your current location. Please go into Settings and enable location services for this app.")
        
        //if not detect user location, set malaysia as default coordinate
        let defaultLocation = CLLocation(latitude: 4.2105, longitude: 101.9758)
        prefs.set(defaultLocation.coordinate.latitude, forKey: "sLatitude")
        prefs.set(defaultLocation.coordinate.longitude, forKey: "sLongitude")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let currentLocation = locations.last  {
            prefs.set(currentLocation.coordinate.latitude, forKey: "sLatitude")
            prefs.set(currentLocation.coordinate.longitude, forKey: "sLongitude")
        } else {
            //if not detect user location, set malaysia as default coordinate
            let defaultLocation = CLLocation(latitude: 4.2105, longitude: 101.9758)
            prefs.set(defaultLocation.coordinate.latitude, forKey: "sLatitude")
            prefs.set(defaultLocation.coordinate.longitude, forKey: "sLongitude")
        }
        
        self.getWeatherData{ (data) in
            self.weatherData = data
            self.locationManager.stopUpdatingLocation() //stop updating location once we get the user location
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - HomePageHeader Delegate
extension HomePageTableViewController: HomePageHeaderDelegate{
    func changeFormat(value: Int){
        switch value{
        case 1:
           prefs.set("fahrenheit", forKey: "sTemperatureFormat")
        default:
           prefs.set("celsius", forKey: "sTemperatureFormat")
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
