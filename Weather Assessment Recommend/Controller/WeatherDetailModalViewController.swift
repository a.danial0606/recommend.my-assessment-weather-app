//
//  WeatherDetailModalViewController.swift
//  Weather Assessment Recommend
//
//  Created by Danial Fajar on 03/02/2020.
//  Copyright © 2020 Danial Fajar. All rights reserved.
//

import UIKit

class WeatherDetailModalViewController: UIViewController {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var weatherIconImage: UIImageView!
    @IBOutlet weak var temperatureLbl: UILabel!
    @IBOutlet weak var weatherStatusLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var windTitleLbl: UILabel!
    @IBOutlet weak var windSpeedLbl: UILabel!
    @IBOutlet weak var humadityTitleLbl: UILabel!
    @IBOutlet weak var humadityValueLbl: UILabel!
    
    var dataView: WeatherDataView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupLabel()
        self.setupView()
        self.configureData(data: self.dataView)
        
        //add user tap gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    // MARK: - Function
    //setup label
    func setupLabel(){
        self.humadityTitleLbl.text = "Humidity:"
        self.windTitleLbl.text = "Wind Speed:"
    }
    
    //setup view
    func setupView(){
        //add shadow effect
        self.weatherIconImage.layer.shadowColor = UIColor.black.cgColor
        self.weatherIconImage.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.weatherIconImage.layer.shadowRadius = 5.0
        self.weatherIconImage.layer.shadowOpacity = 0.7
        
        self.parentView.layer.cornerRadius = 10.0
        
        self.temperatureLbl.layer.shadowColor = UIColor.black.cgColor
        self.temperatureLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.temperatureLbl.layer.shadowRadius = 5.0
        self.temperatureLbl.layer.shadowOpacity = 0.7
        
        self.weatherStatusLbl.layer.shadowColor = UIColor.black.cgColor
        self.weatherStatusLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.weatherStatusLbl.layer.shadowRadius = 5.0
        self.weatherStatusLbl.layer.shadowOpacity = 0.7
        
        self.cityLbl.layer.shadowColor = UIColor.black.cgColor
        self.cityLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.cityLbl.layer.shadowRadius = 5.0
        self.cityLbl.layer.shadowOpacity = 0.7
        
        self.dayLbl.layer.shadowColor = UIColor.black.cgColor
        self.dayLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.dayLbl.layer.shadowRadius = 5.0
        self.dayLbl.layer.shadowOpacity = 0.7
        
        self.windTitleLbl.layer.shadowColor = UIColor.black.cgColor
        self.windTitleLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.windTitleLbl.layer.shadowRadius = 5.0
        self.windTitleLbl.layer.shadowOpacity = 0.7
        
        self.windSpeedLbl.layer.shadowColor = UIColor.black.cgColor
        self.windSpeedLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.windSpeedLbl.layer.shadowRadius = 5.0
        self.windSpeedLbl.layer.shadowOpacity = 0.7
        
        self.humadityTitleLbl.layer.shadowColor = UIColor.black.cgColor
        self.humadityTitleLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.humadityTitleLbl.layer.shadowRadius = 5.0
        self.humadityTitleLbl.layer.shadowOpacity = 0.7
        
        self.humadityValueLbl.layer.shadowColor = UIColor.black.cgColor
        self.humadityValueLbl.layer.shadowOffset = CGSize(width: 0.75, height: 0.75)
        self.humadityValueLbl.layer.shadowRadius = 5.0
        self.humadityValueLbl.layer.shadowOpacity = 0.7
    }
    
    //Configure Data
    func configureData(data: WeatherDataView?){
        let temperatureFormat = prefs.string(forKey: "sTemperatureFormat") == "celsius" ? "°C" : "°F"
        let celsius = Int(((data?.temperature ?? 0.0) - 273.15))
        let fahrenheit = ((data?.temperature ?? 0.0) - 273.15) * 9/5 + 32
        let iconWeather = data?.icon ?? ""
        
        guard let todayDay = Date().dayOfWeek() else { return }

        self.temperatureLbl.text = prefs.string(forKey: "sTemperatureFormat") == "celsius" ? String(celsius) + temperatureFormat : fahrenheit.formatData(f: ".2") + temperatureFormat
        self.weatherStatusLbl.text = data?.weatherStatus ?? ""
        self.cityLbl.text = data?.cityName ?? ""
        self.dayLbl.text = todayDay
        self.windSpeedLbl.text = String(data?.windSpeed ?? 0.0)
        self.humadityValueLbl.text = String(data?.humadity ?? 0)
        
        guard let url = URL(string: "https://openweathermap.org/img/wn/\(iconWeather)@2x.png") else { return }
        self.downloadImage(from: url)
        
        self.backgroundImage.image = UIImage(named: Application.getBackgroundImage(iconWeather))
    }
    
    //get data weather icon from link
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    //get data weather icon from link
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.weatherIconImage.image = UIImage(data: data)
            }
        }
    }
    
    // MARK: - Event Actions
    @objc func dismissModal(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
}
